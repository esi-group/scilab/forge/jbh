// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function antPath = jbh_getAntPath()
  antPath = getenv("JBH_ANT_HOME", "");
  if antPath <> "" then
    antPath = fullpath(antPath + filesep());
  else
    antPath = [];
  end
endfunction
// =============================================================================
