// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function jdkIncludesPath = jbh_getJdkIncludesPath()
  jdkIncludesPath = [];
  if jbh_getJdkPath() <> [] then
    if getos() == "Windows" then
      jdkIncludesPath = [fullpath(jbh_getJdkPath() + "\include"); ..
                         fullpath(jbh_getJdkPath() + "\include\win32")];
    elseif getos == "Linux" then
      jdkIncludesPath = [fullpath(jbh_getJdkPath() + "\include"); ..
                         fullpath(jbh_getJdkPath() + "\include\linux")];
    else
      jdkIncludesPath = fullpath(jbh_getJdkPath() + "\include";
    end
  end
endfunction
// =============================================================================
