// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function bRes = jbh_createBuildXml(pathtofile, projectname, description, libraryname, classpath)

  if size(pathtofile, "*") <> 1 then
    error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 1);
  end
  
  if type(pathtofile) <> 10 then
    error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 1);  
  end  

  if size(projectname, "*") <> 1 then
    error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 2);
  end
  
  if type(projectname) <> 10 then
    error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 2);  
  end  

  if size(description, "*") <> 1 then
    error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 3);
  end
  
  if type(description) <> 10 then
    error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 3);  
  end  

  if size(libraryname, "*") <> 1 then
    error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 4);
  end
  
  if type(libraryname) <> 10 then
    error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 4);  
  end  
  
  buildxmlcontents = mgetl(fullpath(jbh_getJbhPath() + "/script/build_template.xml");
  
  if isdef('classpath') & ~isempty(classpath) <> [] then
    if type(libraryname) <> 10 then
      error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n")), "jbh_createBuildXml", 5);  
    end  
    classpath = matrix(classpath, size(classpath, "*"), 1);
    classpath = "<pathelement location=""" + classpath + """/>";
  else
    classpath =  "";
  end
  
  buildxmlcontents = strsubst(buildxmlcontents, ..
                              "<!-- __PROJECTNAME_TEMPLATE_REPLACE_HERE__ -->", ..
                              projectname);
                              
  buildxmlcontents = strsubst(buildxmlcontents, ..
                              "<!-- __DESCRIPTION_TEMPLATE_REPLACE_HERE__ -->", ..
                              description);
                              
  buildxmlcontents = strsubst(buildxmlcontents, ..
                              "<!-- __LIBRARYNAME_TEMPLATE_REPLACE_HERE__ -->", ..
                              libraryname);
                             
  buildxmlcontents = strsubst(buildxmlcontents, ..
                              "<!-- __CLASSPATH_TEMPLATE_REPLACE_HERE__ -->"
                              classpath);

  bRes = mputl(buildxmlcontents, fullpath(pathtofile + "/build.xml"));  
endfunction
// =============================================================================
