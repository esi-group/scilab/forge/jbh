// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function antPath = jbh_detectAnt()
  antPath = [];
  ANT_PATH = getenv("ANT_HOME", "");
  if (ANT_PATH <> "") & isdir(ANT_PATH) then
    antPath = fullpath(ANT_PATH + filesep());
  elseif getos() <> "Windows" then
    [p, s] = unix_g("which ant");
    if s == 0 then
      antPath = fullpath(p(1) + filesep());
    end
  else
    if isdir(SCI + "/java/ant") then
      antPath = fullpath(SCI + "/java/ant" + filesep());
    end
  end
endfunction
// =============================================================================
