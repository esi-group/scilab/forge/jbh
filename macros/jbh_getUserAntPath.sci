// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function antPath = jbh_getUserAntPath()
  antPath = [];
  if isfile(SCIHOME + "/jbh_ant_conf.txt") then
    txt = mgetl(SCIHOME + "/jbh_ant_conf.txt");
    readedPath = txt(1);
    if isdir(readedPath) then
      antPath = fullpath(readedPath + filesep());
    else
      warning("Your setting about current ant path is wrong.");
    end
  end
endfunction
// =============================================================================
