// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function jdkPath = jbh_getUserJdkPath()
  jdkPath = [];
  if isfile(SCIHOME + "/jbh_jdk_conf.txt") then
    txt = mgetl(SCIHOME + "/jbh_jdk_conf.txt");
    readedPath = txt(1);
    if isdir(readedPath) then
      jdkPath = fullpath(readedPath + filesep());
    else
      warning("Your setting about current JDK path is wrong.");
    end
  end
endfunction
// =============================================================================
