// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function pathJdk = jbh_detectJdk()
  // =============================================================================
  function fn = jdk_javac_name()
    if getos() == "Windows" then
      fn = "javac.exe";
    else
      fn = "javac";
    end
  endfunction
  // =============================================================================
  function bRes = jdk_find(pathToTest)
    bRes = %F;
    if ~isempty(pathToTest) then
      JAVAC = fullpath(pathToTest + filesep() + "bin" + filesep() + jdk_javac_name());
      if isfile(JAVAC) then
        bRes = %T;
      else
        JAVAC = fullpath(pathToTest + filesep() + ".." + filesep() + "bin" + filesep() + jdk_javac_name());
        if isfile(JAVAC) then
          bRes = %T;
        end
      end
    end
  endfunction
  // =============================================================================
  function java_home = jdk_get_java_home()
    java_home = [];
    if getos() == "Windows" then
      java_home = [];
      r = execstr("java_home = winqueryreg(""HKEY_LOCAL_MACHINE"", ""SOFTWARE\JavaSoft\Java Development Kit\1.6"", ""JavaHome"")", "errcatch");
      if r <> 0 then
        java_home = [];
      end
    elseif getos() == "Darwin" then
      // see http://developer.apple.com/mac/library/qa/qa2001/qa1170.html
      [java_home, r] = unix_g("/usr/libexec/java_home --arch x86_64 --failfast --version 1.6+");
      if r <> 0 then
        java_home = [];
      end
    else
      java_home = system_getproperty("java.home");
    end
  endfunction
  // =============================================================================
  pathJdk = [];
  JAVA_HOME = getenv("JAVA_HOME", "");
  if jdk_find(JAVA_HOME) then
    pathJdk = JAVA_HOME;
  else
    java_home = jdk_get_java_home();
    if jdk_find(java_home) then
      pathJdk = java_home;
    end
  end
  
  if pathJdk <> [] then
    pathJdk = fullpath(pathJdk + filesep());
  end
endfunction
// =============================================================================
