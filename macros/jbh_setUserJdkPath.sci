// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function bRes = jbh_setJdkPath(myJdkPath)
  bRes = %F;
  
  if isempty(myJdkPath) then
    if isfile(SCIHOME + "/jbh_jdk_conf.txt") then
      bRes = deletefile(SCIHOME + "/jbh_jdk_conf.txt");
    end
  else   
    if size(myJdkPath, "*") <> 1 then
      error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A directory name expected.\n")), "jbh_setJdkPath", 1);
    end
  
    if type(myJdkPath) <> 10 then
      error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A directory name expected.\n")), "jbh_setJdkPath", 1);  
    end
  
    if ~isdir(myJdkPath) then
      error(999, msprintf(gettext("%s: Wrong value for input argument #%d: A existing directory name expected.\n")), "jbh_setJdkPath", 1);
    end
    bRes = mputl(myJdkPath, SCIHOME + "/jbh_jdk_conf.txt");
  end
endfunction
// =============================================================================
