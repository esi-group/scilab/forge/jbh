// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function bRes = jbh_initialize()
  bRes = %F;
  if getenv("JBH_INITIALIZE", "0") == "0" then
    // check ant path
    ANT_PATH = getenv("JBH_ANT_HOME", "");
    if ANT_PATH == "" then 
      ANT_PATH = [];
    end
    if jbh_getUserAntPath() == [] then
      if jbh_detectAnt() == [] then
        warning("Ant not detected.");
      else
        ANT_PATH = jbh_detectAnt();
      end
    else
      ANT_PATH = jbh_getUserAntPath();
    end
  
    // Add Ant to PATH environment variable
    if (ANT_PATH == [] | ~isdir(ANT_PATH)) then
       warning("Ant path does not exist.");
    else
      if getenv("ANT_HOME", "") == "" then
        setenv("ANT_HOME", ANT_PATH);
      end
      PATH_ENV = getenv("PATH", "");
      PATH_ENV = ANT_PATH + filesep() + "bin" + system_getproperty('path.separator') + PATH_ENV;
      setenv("PATH", PATH_ENV);
      setenv("JBH_ANT_HOME", ANT_PATH);
    end
  
    // check JDK path
    JDK_PATH = getenv("JBH_JDK_HOME", "");
    if JDK_PATH == "" then 
      JDK_PATH = [];
    end
    if jbh_getUserJdkPath() == [] then
      if jbh_detectJdk() == [] then
        warning("JDK not detected.");
      else
        JDK_PATH = jbh_detectJdk();
      end
    else
      JDK_PATH = jbh_getUserJdkPath();
    end
  
    // Add Ant to PATH environment variable
    if (JDK_PATH == [] | ~isdir(JDK_PATH)) then
      warning("JDK path does not exist.");
    else
      if getenv("JAVA_HOME", "") == "" then
        setenv("JAVA_HOME", JDK_PATH);
      end
      PATH_ENV = getenv("PATH", "");
      PATH_ENV = JDK_PATH + system_getproperty('path.separator') + PATH_ENV;
      setenv("PATH", PATH_ENV);
      setenv("JBH_JDK_HOME", JDK_PATH);
    end
    setenv("JBH_INITIALIZE", "1");
    bRes = %T;
  end
endfunction
// =============================================================================
