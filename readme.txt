readme.txt of jbh module

jbh: java build helpers functions

The aim of this module is to simplify build of java file on multiplatforms.

jbh_callAnt - call ANT
jbh_getJdkPath - get JDK path used
jbh_getAntPath - get ANT path used
jbh_getJbhPath - get path of this current module.

jbh_setUserJdkPath - set JDK path
jbh_setUserAntPath - set ANT path
jbh_getUserJdkPath - get user JDK path
jbh_getUserAntPath - get user ANT path

jbh_detectJdk - try to detect JDK path
jbh_detectAnt - try to detect ANT path

jbh_createBuildXml - create a minimal build.xml 

Allan CORNET - 2011